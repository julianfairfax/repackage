#!/bin/bash

if [[ -z "$maintainer_name" ]]; then
	maintainer_name="Julian Fairfax"
fi
if [[ -z "$maintainer_email" ]]; then
	maintainer_email="juliannfairfax@protonmail.com"
fi

cd "${0%/*}"

name="repackage"
version="1.0.1"
description="Linux command line tool to convert packages from the AppImage and snap formats to the Arch, Debian, and RPM package formats"

install -d $name/usr/bin

cp $name.sh $name/usr/bin/$name

chmod +x $name/usr/bin/$name
mkdir $name/DEBIAN

echo "Package: $name
Version: $version
Architecture: all
Maintainer: $maintainer_name <$maintainer_email>
Description: $description
Section: misc
Priority: extra" | tee $name/DEBIAN/control

dpkg-deb -Z xz -b $name/ .

rm -r $name