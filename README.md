[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Repackage
Repackage is a Linux command line tool to convert packages from the AppImage and snap formats to the Arch, Debian, and RPM package formats

## Supported Operating Systems
- Arch-based Linux distributions
- Debian-based Linux distributions
- RPM-based Linux distributions

## Supported AppImages and snaps
In theory all AppImages and user snaps, that contain a single binary sharing the AppImage or snap's name, in the root directory should be supported but server snaps, that have multiple binaries not sharing the snap's name, in multiple directories are not currently supported. If the source code for an AppImage or snap is available, it's recommend to build an Arch, Debian, or RPM package yourself but Repackage is perfect for closed source software! 😉

## Installation
Repackage can be manually download and run from the GitLab releases page or downloaded and installed from the GitLab CI page.

You can also download it from the Arch, Debian, and RPM repositories that are now available [here](https://gitlab.com/julianfairfax/package-repo).

## Usage
Repackage supports downloading a snap, converting it, and copying it to the current working directory using the `download` option or installing an AppImage or snap using the `install` option. Converting snaps requires snapd to be installed. The script will install snapd automatically if you try to convert a snap and it is not installed.

### Step 1

Download the latest version of Repackage from the GitLab releases page.

### Step 2

Unzip the download and open Terminal. Type `chmod +x` and drag the `repackage.sh` script to Terminal. Enter `download` to download, convert, and copy the snap to the current working directory or `install` to install the AppImage or snap, then drag the AppImage to the terminal or enter the name of your snap, the snap channel if necessary, then and hit enter.

## Building
If you want to build Repackage you can do so by running one of the `build.sh` scripts.

## Docs
If you want to know how Repackage converts AppImages, you can check [this doc](https://julianfairfax.gitlab.io/documentation/converting-appimages-to-another-format.html). If you want to know how it converts snaps, you can check [this doc](https://julianfairfax.gitlab.io/documentation/converting-snaps-to-another-format.html) I wrote about it. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.
